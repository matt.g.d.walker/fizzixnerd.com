---
title: idoc Link Checking
---

As most of my friends will know, I've been working on a new markup language for
use in the website for Independent Learning in Science (ILS) called
[idoc](https://gitlab.com/matt.g.d.walker/idoc). I describe here one of the
important unique features of the language: link checking. But first, I need to
talk about the three types of links in idoc.

## Internal Links

Internal links are links to objects within the same document. There are no
special restrictions on these. You can create IDs to links to by using the
syntax

```
[[#locationName]]
```

after the object you wish to link to. You can then refer to that object by using
the syntax

```
<<#locationName>>{display text}
```

## Back Links

Back Links are where things get a little more interesting. In ILS-flavoured
idoc, you are not allowed to link anywhere outside of a document by default.  Instead, you must declare at the top of a document where you wish to be allowed to link to in a prerequisite block called *prerex*.

```
@prerex
---
/Some/Other/Article
---
```

Assuming the above block appears at the top of your document, you can then link
to an object in the other article by using the syntax:

```
<</Some/Other/Article#locationWithinThatArticle>>
```

If you do not declare the article in a prerex block, the document will be
rejected by idoc with an error message specifying which link was not allowed.
The reason you might want to do something like this is to ensure that you are
not referencing information the reader does not yet know. More on this in
another blog post. The point is that by defining and instance of `checkLinks` on
your document type, you can enforce arbitrary constraints of links on your
documents.


## Out Links

These are similar to back links, except they refer to websites outside your
document graph. The protocol must be specified within a link:

```
<<https://fizzixnerd.com>>
```

is good, while

```
<<fizzixnerd.com>>
```

is bad.

These can be restricted in the same way as back links. The way ILS-flavoured
idoc leverages this is by disallowing out links anywhere in a document except in
certain blocks, where any link is allowed.

```
A paragraph containing a link: <<https://fizzixnerd.com/about>>.  This would be rejected.

@furtherreading
---
I can put any link I want in here, though: <<https://fizzixnerd.com/contact>>.
---
```

By restricting where out links can occur, we ensure the author only references
material inside the document graph (and then only to declared prerex), except
when absolutely necessary.

# Conclusion

That's all I wanted to say about links in idoc for now. In short, restricting
where links can point can ensure your document graph has a special structure
that you can take advantage of later. Thanks for reading!
