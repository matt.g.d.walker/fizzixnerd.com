---
title: The Role of Management (An Engineer's Perspective) [Part 2]
---

In the previous post, I talked about an example where a buggy feature got merged
into production, and users didn't like it. Take a look at the previous post for
more details, but I stopped at the point of **delegation** of the planned
responsibilities to members of the team.

## Example (Continued)

Last post, I mentioned:

> There are two sides to the coin of good management: getting out of the way
> when help isn't needed, and helping out when it is.

Now that responsibilities have been delegated, it is the job of management to
get out of the way and let the team do what they do best: engineer a solution to
a problem. What this means _precisely_ is a bit nebulous and depends on the
strengths and limitations of the team members in question, but what I mean is
that _additional overhead_ should be not be introduced at this stage. If the
manager has meetings twice a week with a team member normally, those should
continue, but they should not be upped to, say, three times a week due to this
situation.

The next step is **delivery**, where the team brings what they've accomplished
to another meeting set by management, and the metrics are applied by management
to decide if the problem has been solved. The role of management is again to
tell the team if _expectations_ are being met or not. From this point, we might
return to decomposition, if the metrics indicate the problem hasn't been solved.
Otherwise we move on to the final step: **divergence**.

At this point, the problem has been solved. We now want to make sure it doesn't
happen again. Divergence is similar to a post-mortem in some ways: the team sits
down with management and talks about the _structural_ causes of the issue --
what does the _business_ need to change to ensure a repeat of a similar
situation does not occur. The team should be told ahead of time of the meeting
that this is the goal, so that they can think about it properly. Ultimately,
this is the most difficult step because it requires the business to be willing
to change, it requires the team to feel comfortable voicing concerns in an
honest way, and it requires the team to be willing to change themselves as well.
These can be difficult things for some people, and so a light touch is usually
required here, but this is what (in my opinion) makes or breaks a great manager.

> The meeting is set, and A, R, and S are given around 48 hours to think about
> the root structural causes of the issue.
>
> Management begins the meeting by asking the most important question: what can
> the _company_ do to make sure this doesn't happen again. Engineer A says they
> felt like like they weren't given enough time to complete the project, and
> that they should have communicated that better. Reviewer R states that they
> feel like their review processes are too ill-defined, and that sometimes they
> weren't sure how much effort they were expected to put into the process. S,
> the more senior reviewer, points out that both are communication issues, and
> states that is what they thinks ultimately broken down in this case:
> communication.

This is typical of a review session from my experience: even though management
has asked how the _company_ can change, the people involved feel like the
failure is on them and offer up ways for _themselves_ to improve. Management
needs to push back at this, in this situation, to try to get the kind of
feedback that has the most value for them.

> The manager explains that this situation could have been avoided by better
> communication perhaps, but that communication is a big problem domain and a
> hard one to solve. They ask again, what the _company_ could have done to make
> sure this doesn't happen again. They give an example: would it be better for A
> if they are asked at the beginning of the task to estimate how much time they
> think it will take, perhaps _before_ management provides the deadlines they
> were planning on making?
>
> S is curious: how _did_ management decide how long it was going to take? The
> manager explains the process of estimation used at the company. The team tries
> to pinpoint an area where they could be more involved, but it is difficult
> because often the work is estimated _before_ it is known which resource(s)
> will be put on it. This means it wouldn't have been necessarily the members of
> the team that were here who were participating in the estimation.
>
> The team talks a little more, realizing that there may be a problem more
> generally in this same vein: there is a disconnect between the different
> stages of the process. Specifically, there was a disconnect between sales and
> developers over the estimation of effort, and a disconnect between developers
> and reviewers over the code itself.

Here the team has identified structural issues, which is good. However, we must
take the monumental step from identification of the problem to proposing a
solution to it. Usually management are in the best position to do this, because
they have a finger on the pulse of the business itself.

> The manager explains that changing the sales and estimation process is much
> more difficult than changing the review process, because of the personalities
> involved. They (the manager) can make the engineer's voice heard at the next
> management meeting, but they can't promise changes will come quickly. They
> note if there was a disconnect between reviewer and developer however, they
> have the power to solve _that_ issue. They propose the following change: their
> review process requires two approvals from reviewers before code is merged
> currently. However, there is no guarantee that these reviewers will be
> involved with the work. When a subproject is started, management proposes that
> no less than 1.1 human resources be placed on it, and that at least one of
> those two humans involved in the subproject will review the code.

That is to say, per one week full development, there must be at least half a day
of involvement of another developer, who will become a reviewer. This ensures
the following: that the engineer developing the subproject is never left totally
alone; that there is budgeted time for a thorough review of the work involving
at least two people; that at least one reviewer will be familiar enough with the
work being done to be able to properly review it without spending a huge amount
of extra time on it.

This requires management to make a commitment, and it allows them to be called
out if they fail to keep to it. This is a good thing: just as management keeps
engineers accountable, the engineers keep management accountable. This solution
is not the end of the road. Continual re-evaluation of these processes is
important, and indeed necessary. For example, it was not well defined what
constitutes a "subproject".

## Summary

* Discovery: When a problem outcome is made known to management.
* Definition: When the problem outcome is made known to the team, and
  expectations are set and metrics of success established.
* Decomposition: When the team brainstorms _actions_ and _estimates of cost_ of
  those actions which are to fix the problem.
* Delegation: When management assigns responsibilities to perform the actions
  approved during decomposition.
* Delivery: When the team presents the results of the actions to management and
  it is discussed whether the problem has been solved.
* Divergence: When management makes a structural change so that the business
  does not reconverge on making the same mistake again.

If I were in marketing, I would come up with a cool name for this process like
**6D Chess** or something, but I'm not.

Finally, management must work with engineers to help them change for the better
too. The reason I left this out of the process though is that this is a much
bigger topic that deserves perhaps its own blog post [series], one day. Suffice
it to say that I don't think it's always the job of the business to change, but
it _is_ always the job of the business to mitigate the risks to itself through
processes while others are being asked to change.

Reflecting a little, I'm not sure I did what I set out to do. I wanted to
explain _what_ management is supposed to do, but I ended up giving a process on
_how_ management should handle problems. I will probably end up writing more on
this later, but until then, it will have to do!
