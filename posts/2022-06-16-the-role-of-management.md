---
title: The Role of Management (An Engineer's Perspective) [Part 1]
---

I have been reflecting on my most recent employments, trying to understand what
I liked and what I didn't like. These are some thoughts about how I believe
management is an important part of a company, and how good management helps me
be more efficient.

Everyone has personal limitations. The primary role of management is to help
_manage_ those limitations and decrease their negative effect on both the person
and the business. Some example limitations can be: difficulty switching tasks
efficiently, difficulty communicating with customers effectively, putting too
much pressure on oneself, not holding one's work to a high enough standard,
difficulty prioritizing more important work over less important work, forgetting
to keep people abreast of the work they are doing, etc. This wide array of
different needs means that a one-size-fits-all approach to management is
inappropriate and in fact harmful; since interventions for each of these
possible limitations introduce overhead, management must tailor their approach
to the strengths and limitations of the individual in question.

What is important to note is that the engineer may not be able to manage these
limitations effectively on their own at the moment. Asking the individual to
improve in the aspects that are limiting them is reasonable and we will get to
that, but there must be a plan in the meantime for how to minimize the negative
effects.

There are two sides to the coin of good management: getting out of the way when
help isn't needed, and helping out when it is. An example is useful.

## Example

> Engineer A has been working on a subsystem for the Big Project, mainly working
> alone, and reviewers R and S (S being more senior) for their pull requests
> have been approving their changes without being directly involved in the work
> A is doing.

A number of issues come up when the subsystem is integrated: in general terms it
is buggy and poorly optimized and does not improve the product. This moment,
when we find out there is a problem, I call **discovery**. It is important to
note when this happens because any plan of action can only begin after
discovery.

What is the role of management in this situation? I believe there are several
things to do. The first is to make the relevant people (in this case, A, R, and
S) aware of the problem, and bring them together to discuss it. I call this
point of contact **definition**. The role of management is to _define the
outcome_ which is occuring which cannot be allowed to continue, and to _give
context on why it's a problem_ ideally _using hard data to exhibit the issue_.
In this case, this could be stated as:

> The app is crashing too often with exceptions raised from the subsystem.
>
> Users are reportedly becoming frustrated with the performance of the app, and
> this has been picked up by a decrease in our satisfaction metrics since the
> release of the subsystem.

This sets expectations, and gives those involved a clear understanding of what
they are being asked to change. If metrics/hard data are used, it also gives
management and those involved a clear understanding of if the problem has been
solved. In other words, the role of the manager during definition is to _set
expectations_ and to explicitly indicate in what way there has been failure
_meet expectations_, providing measures that can be used to determine this in a
uniform way.

---

As an aside: setting _expectations_ does not mean setting _consequences_. This
is a relatively common mistake I've seen from management; the engineer can be
told how important it is that some issue be fixed without being told the
consequences to them should they fail to do so. Engineers are generally smart;
they can recognize that a problem needs to be fixed without being told they will
be fired or removed from the project if they fail to fix it. In fact,
threatening consequences like this serves no positive purpose, and normally only
causes anxiety in the engineer, causing their performance to drop, which is
counter-productive.

---

After definition has been done by the management, it is the responsibility of
the team involved to brainstorm ideas for causes of the issue. This would mainly
be the job of A, R, and S, in this case. I call this process **decomposition**
(yes, I am ensuring they all start with "d" because alliterations are fun),
where the outcome is decomposed into causes that can be addressed with actions.

> Engineer A mentions that they haven't been writing tests for every component
> they write. R asks why not, and A responds that they felt they couldn't
> deliver the product _and_ write the tests with the deadlines given. A says
> that tests can be added over the next week, but that this will delay the
> current work they have started in the meantime.
>
> S asks R how they have been reviewing the code. R explains that they just look
> over the code, and don't download it or try to compile it. If the CI is green,
> they basically feel it's okay to merge it. S suggests that A can schedule an
> "overview and demonstration" session for each non-trivial PR with R and S, so
> that they can see it in action and be more aware of the design decisions A
> made during the development process. R suggests such an event might be helpful
> even now, after the merges, just to get a handle on the situation from a
> technical perspective. This would be a three-person meeting taking 1-2 hours.
>
> A asks how they should handle benchmarking the performance of the app and the
> subsystem. The team brainstorms ways this can be done, estimating the time,
> human, and cloud resources they would require to accomplish this task.
>
> Management provides access to a curated list of recent user reports for the
> team to prioritize working on the parts that are broken. Reading these reports
> would not take very long.

I could list more things they might have thought of, but you get the idea I
think. The point is that during decomposition two things are put forward: an
_action_ and an _estimated cost_ of that action.

It is up to management with the help of the team to decide, from these actions
and estimates, what actual execution should take place. The engineers indicate
what they think would be most helpful, and management makes the cost decisions
on what to approve. Management in this case approves the test writing, the
synchronous overview and demonstration, and the review of user reports, and says
that they will wait on the benchmarking because of the higher cost (and the
others might solve the immediate problem). Responsibilities are clearly
**delegated** to each person; A schedules a meeting with R and S and begins work
on the tests, and each person on the team reads the user reports. Management
adds "benchmark app" and "benchmark subsystem" to the backlog and prioritizes it
according to the businesses needs.

This post is getting a little long, so I think I'll stop here for now. In part
two we will finish off the "d" processes by ensuring the problem has been solved
and then reflecting on how to improve processes so such a situation is less
likely to happen in the future.
