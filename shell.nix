{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell rec {
  buildInputs = [ (pkgs.haskellPackages.ghcWithPackages (hpkgs : with hpkgs; [ zlib cabal-install])) pkgs.glibc pkgs.gnupg ];
  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;
  LD_PRELOAD = "${pkgs.glibc.outPath}/lib/libc.so.6";
}
