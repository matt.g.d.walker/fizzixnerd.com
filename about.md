---
title: About
headerImg: beach.jpg
---

I am a software engineer from Canada. I'm interested in type theory and
functional programming, and their applications to creating robust software and
rock-solid infrastructure. In a previous life I was aiming to be a
physicist/mathematician. You can send me an email
[here](mailto:matt@fizzixnerd.com), or take a look at my GitHub
[here](https://www.github.com/Fizzixnerd).

You can download my CV [here](pdfs/resume.pdf).

